import { ApolloServer } from "@apollo/server";
import { startStandaloneServer } from "@apollo/server/standalone";
import { productsMock, categoriesMock } from "./mocks/product";
import { mockAddresses } from "./mocks/address";
import { typeDefs } from "./schema/index";
import { Category } from "./resolver/Category";
import { Query } from "./resolver/Query";

const resolvers = {
  Query,
  Category,
};

interface MyContext {
  productsMock?: any;
  categoriesMock: any;
  mockAddresses: any;
}

async function startApolloServer() {
  const server = new ApolloServer<MyContext>({ typeDefs, resolvers });
  const { url } = await startStandaloneServer(server, {
    context: async () => ({
      productsMock,
      categoriesMock,
      mockAddresses
    }),
    listen: { port: 8095 },
  });
  console.log(`🚀  Server ready at ${url}`);
}

startApolloServer();

// query {
//   address(id: "12345") {
//     geolocation
//     surburb
//   }
// }
