export const mockAddresses = [
    {
      id: "12345",
      unitNumber: 1,
      number: 22,
      name: "Collins St",
      surburb: "Melbourne",
      geolocation: "123.212.1212.121212",
      city: "Melbourne",
      zip: 3001
    },
    {
      id: "234456",
      number: 500,
      name: "Bourke St",
      surburb: "Melbourne",
      geolocation: "123213.21212",
      city: "Melbourne",
      zip: 3000
    },
  ];