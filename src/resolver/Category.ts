export const Category = {
  products: (parent: any, args: any, { productsMock}: any) => {
    console.log("parent", parent);
    const categoryId = parent.id;
    return productsMock.filter((p: any) => p.categoryId === categoryId);
  },
};
