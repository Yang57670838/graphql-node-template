export const Query = {
  addresses: (parent: any, arg: any, context: any) => context.mockAddresses,
  address: (parent: any, arg: any, context: any) => {
    console.log("arg", arg);
    const addressID = arg.id;
    return context.mockAddresses.find((a: any) => a.id === addressID);
  },
  products: (parent: any, arg: any, { productsMock }: any) => {
    return productsMock;
  },
  categories: (parent: any, arg: any, { categoriesMock }: any) => {
    return categoriesMock;
  },
  category: (parent: any, arg: any, { categoriesMock }: any) => {
    return categoriesMock.find((c: any) => c.id === arg.id);
  },
};
