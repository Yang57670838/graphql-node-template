
// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
export const typeDefs = `#graphql
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  # This "Book" type defines the queryable fields for every book in our data source.
  type Address {
    id: ID!
    unitNumber: Int
    number: Int!
    name: String!
    surburb: String!
    geolocation: String!
    city: String!
    zip: Int!
  }

  type Product {
    id: ID!
    name: String!
    description: String!
    image: String
    quantity: Int!
    price: Float!
    onScale: Boolean!
  }

  type Category {
    id: ID!
    name: String!
    products: [Product!]!
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "books" query returns an array of zero or more Books (defined above).
  type Query {
    addresses: [Address!]!
    address(id: ID!): Address
    products: [Product!]!
    categories: [Category!]!
    category(id: ID!): Category
  }
`;